window.addEventListener('load', function() {
	//stran nalozena

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			if (cas == 0) {
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert(`Opomnik!\n\nZadolžitev ${naziv_opomnika} je potekla!`);
				document.querySelector("#opomniki").removeChild(opomnik);
			} else {
				casovnik.innerHTML = (cas - 1);
			}

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);

	var prijaviSe = function () {
		var uporabnik = document.querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML = uporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	}
	document.querySelector("#prijavniGumb").addEventListener("click", prijaviSe);

	var dodajOpomnik = function() {
		var naziv_opomnika = document.querySelector("#naziv_opomnika");
		var cas_opomnika = document.querySelector("#cas_opomnika");

		document.querySelector("#opomniki").innerHTML += `<div class='opomnik senca rob'><div class='naziv_opomnika'>${naziv_opomnika.value}</div><div class='cas_opomnika'> Opomnik čez <span>${cas_opomnika.value}</span> sekund.</div></div>`;

		naziv_opomnika.value = "";
		cas_opomnika.value = "";
	};
	document.querySelector("#dodajGumb").addEventListener("click", dodajOpomnik);

});
